# A library of music loops
This is a set of music loops for Foundry VTT

# Activate the module
After installing, as GM go to the `Manage Modules` options menu in your World Settings tab then enable the `Wyndric's Tome of Magic Music` module.

# Usage

The Playlist will then show up under `Compendium Packs`

Open Wyndric's Tome of Magic Music folder in the Compendium and drag the playlists into the Audio Playlist section.
